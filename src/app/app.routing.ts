import { RouterModule, Routes, NoPreloading, PreloadingStrategy, RouteReuseStrategy} from '@angular/router'
import { PlaylistsComponent } from './playlists/playlists.component';

const routes: Routes = [
  { path: '', redirectTo:'playlists', pathMatch:'full' },
  { path: 'playlists', component: PlaylistsComponent },
  // { path: 'music', loadChildren: './music/music.module#MusicModule' },
  { path: '**', redirectTo:'playlists', pathMatch:'full' },
]

export const Routing = RouterModule.forRoot(routes, {
  // enableTracing: true,
  useHash: true,
  preloadingStrategy: NoPreloading // PreloadingStrategy
  
})