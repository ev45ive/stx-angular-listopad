import { TestBed, inject } from '@angular/core/testing';

import { PlaylistResolverService } from './playlist-resolver.service';

describe('PlaylistResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlaylistResolverService]
    });
  });

  it('should be created', inject([PlaylistResolverService], (service: PlaylistResolverService) => {
    expect(service).toBeTruthy();
  }));
});
