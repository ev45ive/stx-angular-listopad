import { Attribute, Directive, ElementRef, HostBinding, HostListener, Input, Renderer2 } from '@angular/core'

@Directive({
  selector: '[appHighlight]',
  // host:{
  //   '(mouseenter)':'hoverIn($event)'
  // }
})
export class HighlightDirective {

  constructor() { }

  @HostBinding('style.borderLeftColor')
  color
  
  updateColor() {
    // console.log('getting color....')
    this.color = this.hover ? this.highlightColor : 'transparent'
  }

  hover = false

  @HostListener('mouseenter')
  hoverIn() {
    this.hover = true
    this.updateColor()
  }

  @HostListener('mouseleave')
  hoverOut() {
    this.hover = false
    this.updateColor()
  }

  @Input('appHighlight')
  set setColor(color){
    this.highlightColor = color
    this.updateColor()
  }
  
  highlightColor = 'transparent'

  ngOnInit() {
    this.updateColor()
  }
}

window['HighlightDirective'] = HighlightDirective
