import { catchError, map, switchMap, tap } from 'rxjs/operators'
import { PlaylistsService } from './playlists.service'
import { Component, OnInit, Input } from '@angular/core';
import { Playlist } from './playlist';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styles: []
})
export class PlaylistsComponent implements OnInit {

  playlists: Playlist[] = [
  ]

  playlists$

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService) {


    this.selected$ = this.route.firstChild.paramMap.pipe(
      map(paramMap => parseInt(paramMap.get('playlist_id'), 10)),
      switchMap(id => this.playlistsService.getPlaylist(id))
    )
  }


  selected$
  message

  select(playlist) {
    this.router.navigate(['/playlists', playlist.id])
  }

  ngOnInit() {
    this.playlists$ = this.playlistsService.getPlaylists()
      .pipe(
      // catchError()
      tap(() => this.message = 'Success'),
      tap(playlists => this.playlists)
      )
  }

}
