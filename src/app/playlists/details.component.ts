import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { Playlist } from './playlist';

enum MODES { show = 'show', edit = 'edit' }

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styles: []
})
export class DetailsComponent implements OnInit {

  @Input()
  playlist: Playlist

  draft:Playlist

  MODES = MODES

  mode:MODES = MODES.show

  @Output()
  playlistChange = new EventEmitter<Playlist>()

  save(){
    this.playlistChange.emit(this.draft)
    this.cancel()
  }

  edit(){
    this.draft = Object.assign({},this.playlist)
    // this.draft = this.playlist
    this.mode = MODES.edit
  }

  cancel(){
    this.mode = MODES.show
  }

  constructor() { }

  ngOnInit() {
  }

}
