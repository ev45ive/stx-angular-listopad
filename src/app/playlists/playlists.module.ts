import { HttpClient } from '@angular/common/http'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistsComponent } from './playlists.component';
import { ItemsComponent } from './items.component';
import { ItemComponent } from './item.component';
import { DetailsComponent } from './details.component';
import { HighlightDirective } from './highlight.directive';
import { PlaylistsService } from './playlists.service';
import { HttpClientModule } from '@angular/common/http';
import { PlaylistContainerComponent } from './playlist-container.component';
import { PlaylistResolverService } from './playlist-resolver.service';


const routes: Routes = [
  {
    path: 'playlists', component: PlaylistsComponent,
    resolve:{
      'selected': PlaylistResolverService
    },
    runGuardsAndResolvers:'always',
    data:{
      'placki':'bo lubieee'
    },
    children: [
      { path:'', component: PlaylistContainerComponent },
      { path:':playlist_id', component: PlaylistContainerComponent,
      // resolve:{
      //   'selected': PlaylistResolverService
      // },
     },
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    PlaylistResolverService,
    PlaylistsService
  ],
  declarations: [
    PlaylistsComponent,
    ItemsComponent,
    ItemComponent,
    DetailsComponent,
    HighlightDirective,
    PlaylistContainerComponent,
  ]
})
export class PlaylistsModule { }
