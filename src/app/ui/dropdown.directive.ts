import { ContentChild, Directive, ElementRef, HostListener } from '@angular/core'
import { DropdownMenuDirective } from './dropdown-menu.directive';
import { DropdownToggleDirective } from './dropdown-toggle.directive';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  constructor(private elem: ElementRef) { }

  @ContentChild(DropdownMenuDirective
    /* , { read: DropdownMenuDirective } */
  )
  menuRef: DropdownMenuDirective

  @ContentChild(DropdownToggleDirective)
  toggleRef: DropdownToggleDirective

  // After all view directives had their ngOnInit
  ngAfterContentInit() {

    this.toggleRef.toggled.subscribe(() => {
      this.toggle()
    })
  }

  @HostListener('document:keyup.escape')
  escape(){
    this.close()
  }

  @HostListener('document:click', ['$event.path'])
  blur(path) {
    if (path.indexOf(this.elem.nativeElement) === -1) {
      this.close()
    }
  }

  opened = false

  open() {
    this.opened = true
    this.menuRef.setOpened(this.opened)
  }

  close() {
    this.opened = false
    this.menuRef.setOpened(this.opened)
  }

  toggle() {
    this.opened = !this.opened
    this.menuRef.setOpened(this.opened)
  }

}
