import { Component, Inject, Input, OnInit } from '@angular/core'
import { Album } from './album';
import { MusicService } from './music.service';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-albums-list',
  templateUrl: './albums-list.component.html',
  styles: [`
    .card{
      min-width:25%;
      max-width:25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums

  constructor(){}

  ngOnInit() {
  }
  
}
