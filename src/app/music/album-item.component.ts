import { Component, Input, OnInit } from '@angular/core'
import { Album, AlbumImage } from './album';

@Component({
  selector: 'app-album-item',
  templateUrl: './album-item.component.html',
  styles: []
})
export class AlbumItemComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input('album')
  set onAlbum(album){
    this.album = album;
    this.image = album.images[0]
  }

  album:Album

  image:AlbumImage


}
