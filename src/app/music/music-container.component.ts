import { MusicService } from './music.service'
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-music-container',
  template:`<ng-content></ng-content>`,
  styles: [],
  providers:[
    MusicService
  ]
})
export class MusicContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
